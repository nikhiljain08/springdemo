package com.codeondev.demoapi.v1;

import org.springframework.data.repository.CrudRepository;

public interface TopicRespository extends CrudRepository<Topic, String>{

}
