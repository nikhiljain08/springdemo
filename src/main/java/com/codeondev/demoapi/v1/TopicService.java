package com.codeondev.demoapi.v1;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TopicService {
	
	@Autowired
	private TopicRespository topicRespository;
	
	public List<Topic> getAllTopics() {
		List<Topic> topics = new ArrayList<>();
		topicRespository.findAll().forEach(topics::add);
		return topics;
	}
	
	public void addTopic(Topic topic) {
		topicRespository.save(topic);
	}
	
}
